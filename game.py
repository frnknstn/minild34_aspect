#!/usr/bin/python
"""MiniLD 34 - Aspect

"""

from __future__ import division
from __future__ import print_function

import os
import sys
import random
import time
import itertools

import pygame

from defines import *
from level import Level
from viewport import Viewport
import sprite_types

class FramerateCounter(object):
    buffer_size = 60
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame.font.SysFont("", 16)


    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        if sys.platform.startswith('linux'):
            this_time = time.time()
        else:
            this_time = time.clock()
        this_interval = (this_time - self.last_time)

        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self, surf):
        surf.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))


class Game(object):
    def handle_trigger(self, trigger):
        """One of our sprites hit an unknown trigger!"""

        print("One of our sprites hit an unknown trigger!")

    def main(self):
        debug("Game.main() running")

        if not DOUBLE_SIZE:
            screen = pygame.display.set_mode(SCREEN_SIZE)
        else:
            real_screen = pygame.display.set_mode(
                (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
            screen = pygame.Surface(SCREEN_SIZE)

        clock = pygame.time.Clock()

        level = Level("map/animassault.tmx")
        tiles = level.images

        sprite_types.load_sprite_files()

        players = []
        sprites = []        # all sprites and things, including the players
        viewports = []

        pygame.INFO_SPRITE_COUNT = 0

        # prepare the stuff
        if not BENCHMARK:
            for player_id, trigger in level.player_starts.iteritems():
                player_start = trigger.level_pos
                debug("Creating player %d at %s" % (player_id, str(player_start)))
                player = sprite_types.PlayerMob(player_start)
            players.append(player)
            sprites.append(player)
            sprites.append(sprite_types.SquirrelMob((260,200), "left"))
            
            player.dx = PLAYER_SPEED

            viewport = Viewport(level, (0, 0), (0, 0), (SCREEN_SIZE[0], SCREEN_SIZE[1]), target=player)
            viewport.draw(screen, sprites)
            move = (0, 0)

            viewports.append(viewport)
        else:
            # benchmark stuff
            moves = []
            last_rects = []
            
            for i in range(SCREEN_SIZE[0] // PANEL_WIDTH):
                viewport = Viewport(level, 
                    (random.randint(0, level.size[0]), random.randint(0, level.size[1])),
                    (i * PANEL_WIDTH, 0), 
                    (PANEL_WIDTH, SCREEN_SIZE[1]))
                viewport.draw(screen)
                viewports.append(viewport)
                moves.append( (random.randint(-1, 1), (random.randint(-1, 1))) )
                last_rects.append(viewport.rect)

            player = sprite_types.PlayerMob((200,200))
            players.append(player)
            sprites.append(player)
            player.dx = 1.5
            viewports[0].target = player

            for i in range(1000):
                pos = (random.randint(0, level.size[0]), random.randint(0, level.size[1]))
                direction = random.choice(("left", "right"))
                new_sprite = random.choice((sprite_types.PlayerMob, sprite_types.SquirrelMob))(pos, direction)
                new_sprite.dx = (random.random() - 0.5)
                new_sprite.dy = (random.random() - 0.5)
                sprites.append(new_sprite)

            for i in range(1, len(viewports)):
                viewports[i].target = sprites[i]
                
            

        pygame.display.flip()


        frame_count = 0
        fps = FramerateCounter()
        info_font = pygame.font.SysFont("", 16)

        # Main Loop
        running = True

        while running:

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key in (pygame.K_ESCAPE, pygame.K_q):
                        running = False

                    # movement
                    elif event.key == pygame.K_UP:
                        move = (move[0], move[1] - 2)
                    elif event.key == pygame.K_RIGHT:
                        move = (move[0] + 2, move[1])
                        viewport.move_view_by(1, 0)
                    elif event.key == pygame.K_DOWN:
                        move = (move[0], move[1] + 2)
                    elif event.key == pygame.K_LEFT:
                        move = (move[0] - 2, move[1])

                    else:
                        debug(event)

                elif event.type == pygame.KEYUP:
                    # movement
                    if event.key == pygame.K_UP:
                        move = (move[0], move[1] + 2)
                    elif event.key == pygame.K_RIGHT:
                        move = (move[0] - 2, move[1])
                        viewport.move_view_by(1, 0)
                    elif event.key == pygame.K_DOWN:
                        move = (move[0], move[1] - 2)
                    elif event.key == pygame.K_LEFT:
                        move = (move[0] + 2, move[1])


                if event.type == pygame.QUIT:
                    running = False

            # Update all the sprites
            for sprite in sprites:
                sprite.update_animation()
                trigger = sprite.move(level)
                if trigger is not None:
                    self.handle_trigger(trigger)

            # Update all the viewports
            if not BENCHMARK:
                for viewport in viewports:
                    viewport.move_view_by(*move)
                    viewport.update()
                    viewport.draw(screen, sprites, level.triggers)
            else:
                for index in range(len(viewports)):
                    viewport, move, last_rect = viewports[index], moves[index], last_rects[index]

                    if viewport.target is None:
                        viewport.move_view_by(*move)
                        if viewport.rect == last_rect:
                            viewport.move_view((
                                (random.randint(0, level.size[0]), random.randint(0, level.size[1])),
                                (PANEL_WIDTH, SCREEN_SIZE[1]) ))
                            moves[index] = (random.randint(-1, 1), (random.randint(-1, 1)))
                        
                    viewport.update()
                    viewport.draw(screen, sprites, level.triggers)

                    last_rects[index] = viewport.rect


            frame_count += 1
            fps.frame()

            if DEBUG:
                # draw debug info
                fps.draw(screen)
                info = "%dx%d window, %d viewports, %d sprites (%d drawn), %d frames" % (
                    SCREEN_SIZE[0], SCREEN_SIZE[1], len(viewports), len(sprites), pygame.INFO_SPRITE_COUNT,
                    frame_count
                    )
                screen.blit(info_font.render(info, True, (255,255,255), (0,0,0)), (0, 12))
                pygame.INFO_SPRITE_COUNT = 0


            if DOUBLE_SIZE:
                tmp_surf = pygame.transform.scale(screen, (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2), real_screen)
                #real_screen.blit(tmp_surf)


            pygame.display.flip()


            if CAP_FPS is not None:
                clock.tick(CAP_FPS)
            else:
                time.sleep(0.0005)
            

        debug("Leaving main loop")
        screen.fill((0, 0, 0))

        pygame.display.flip()

def main():
    try:
        pygame.init()
        game = Game()
        game.main()
    finally:
        debug("Stopping pygame...")
        pygame.quit()

    debug("Exiting.")


if __name__ == "__main__":
    main()
