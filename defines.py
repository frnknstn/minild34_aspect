#!/usr/bin/python
from __future__ import division
from __future__ import print_function

DEBUG = False
DEBUG = True

BENCHMARK = False
#BENCHMARK = True

CAP_FPS = 60
CAP_FPS = 90
#CAP_FPS = None

SCREEN_SIZE = (960, 128)
SCREEN_SIZE = (960, 480)
#SCREEN_SIZE = (240, 480)


DOUBLE_SIZE = False
#DOUBLE_SIZE = True

PANEL_WIDTH = 120

TILE_SIZE = 32

TILE_DIR = "tiles"
SPRITE_DIR = "sprites"

DIRECTIONS = ("none", "up", "right", "down", "left")
# D_NONE = 0
# D_UP = 1
# D_RIGHT = 2
# D_DOWN = 3
# D_LEFT = 4

# default walking speed of player
PLAYER_SPEED = 1.5

def debug(*args):
    if DEBUG:
        print(*args)


if __name__ == "__main__":
    import game
    game.main()
