#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import itertools
import array
import pygame

from pytmx import tmxloader

from defines import *

class Trigger(object):
    """Map trigger object"""

    # properties we are expecting to be integers:
    INT_PROPERTIES = set(("player", "player_id"))
    
    def __init__(self, obj):
        """Create a map trigger object from a supplied dict of object properties from the TMX map."""
        for key, value in obj.iteritems():
            if key in self.INT_PROPERTIES:
                value = int(value)

            setattr(self, key, value)

        # for some reason, it seems that object tiles use the bottom-left corner, not the top left
        self.y -= TILE_SIZE

        self.rect = pygame.Rect((self.x, self.y), (TILE_SIZE, TILE_SIZE))
        self.level_pos = (self.x, self.y)
        self.tile_pos = (self.x // TILE_SIZE, self.y // TILE_SIZE)

        debug("Created trigger tile '%s' at %dx%d" %(self.type, self.x, self.y))


class Level(object):
    def __init__(self, filename):
        debug("Loading TMX map '%s'" % filename)
        self.filename = filename

        tmx = tmxloader.load_pygame(filename)

        self.images = tmx.images
        self.size = (tmx.width * TILE_SIZE, tmx.height * TILE_SIZE)
        self.tile_size = (tmx.width, tmx.height)
        self.rect = pygame.Rect((0,0), self.size)

        self.data = self.load_map_data(tmx)

        # prepare all the triggers, control data and objects
        self.trigger_types = {}
        self.triggers = []

        self.player_starts = {}

        # all my triggers are in object layer 0
        objects = tmx.objectgroups[0].objects

        for gid, properties in tmx.tile_properties.items():
            properties["gid"] = gid
            debug("Found control tile type %d: %s" % (gid, str(properties)))
            self.trigger_types[gid] = properties

        for trigger in objects:
            self.add_trigger(trigger)


        debug("TMX Map is %dx%d pixels big (%dx%d tiles)" % (self.size[0], self.size[1], 
                                                             self.tile_size[0], self.tile_size[1]))

    def add_trigger(self, trigger):
        """Parse a new control trigger tile"""
        gid = trigger.gid

        # override our properties with those from the trigger type
        for key, value in self.trigger_types[gid].items():
            setattr(trigger, key, value)

        # check our validity
        if not self.valid_trigger(trigger.__dict__):
            debug("Warning: Not parsing invalid trigger %s" % str(trigger))
            return False

        # convert us to the internal Trigger object
        trigger = Trigger(trigger.__dict__)

        # add us!
        self.triggers.append(trigger)

        # type-specific checks
        if trigger.type == "player start":
            self.player_starts[trigger.player] = trigger

        return True


    def valid_trigger(self, trigger):
        """check the trigger type is valid, with no obviously bogus information."""
        # generic tests
        try:
            trig_type = trigger["type"]
        except KeyError:
            debug("trigger has no type!")
            return False

        if "direction" in trigger:
            direction = trigger["direction"]
            if direction not in DIRECTIONS:
                debug("trigger '%s' has an invalid direction '%s'" % (trig_type, direction))
                return False

        # trigger-specific tests
        if trig_type == "player start":
            try:
                player_id = int(trigger["player"])
            except KeyError:
                debug("trigger '%s' has no player property" % (trig_type))
                return False
            except ValueError:
                debug("trigger '%s' has invalid player ID '%s'" % (trig_type, trigger["player"]))
                return False

        elif trig_type == "player direction":
            if "direction" not in trigger:
                debug("trigger '%s' has no direction" % trig_type)
                return False

        elif trig_type == "story":
            if "direction" not in trigger:
                debug("trigger '%s' has no direction" % trig_type)
                return False
            if "text" not in trigger:
                debug("trigger '%s' has no text" % (trig_type, text))
                return False

        return True

    def load_map_data(self, tmx):
        # all my actual map data is in tile layer 0
        data = tmx.tilelayers[0].data

        # swap the x and y tile axis, so I can address the data in a more reasonable fashion :)
        x_size = len(data)
        y_size = len(data[0])

        new_data = [ array.array(data[0].typecode, [0] * y_size) for x in range(x_size) ]

        for x, y in itertools.product(range(x_size), range(y_size)):
            new_data[x][y] = data[y][x]

        return new_data



    def __repr__(self):
        return "<%s: '%s' 0x%X>" % (self.__class__.__name__, self.filename, id(self))


if __name__ == "__main__":
    import game
    game.main()
